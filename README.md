# Frontend Mentor - NFT preview card component solution

### About Project
This is a solution to the [NFT preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/nft-preview-card-component-SbdUL_w0U).

This project uses **HTML** and **vanilla CSS** to implement a *mobile first*, *responsive* design. The screenshot of the finished product can be seen below.

![Design screenshot](./Screenshot.png)


### Tech stack:

- [x] HTML to create the structure
- [x] vanilla CSS to design the webpage
- [x] Grid and flex layouts to implement the design.
- [x] BEM naming convention in writing CSS


### Process

- Firstly, to start off the project, I navigated to a newly created directory and created an empty git repo inside using `git init` command. The **FrontEnd Mentor NFT card code challenge** repository is cloned in the created repo.
- Next, all the unused files, inline CSS etc were removed. **Outfit** font family was added to the project using *Google Fonts CDN*. All the colors provided in the *style-guide.md* was added to the `:root` of the CSS. 
- A CSS reset was implemented. Font size of the page was set to 18px so that `1rem == 18px`
- The main page sanning the full viewport with appropriate background color was implemented. 
- The `card` component was implemented using *mobile first* design. The contents were added keeping *responsive design* in mind.
- Added media query to look according to the `designs` using `375px` for `mobile` and `1440px` for `desktop` breakpoints. The static page was tested on various viewports/devices using the Firefox mobile layout viewer. 
- A screenshot of the final result was taken using **Firefox screenshot** and was added to the project.


### Tools used

- VS Code as the Code Editor
- Live server VS Code extension to create a dev server
- Prettier pluggin for code formatting


### Links

- Solution URL: [Gitlab](https://gitlab.com/kanuos/fem-nft-card)
- Live Site URL: [Netlify](https://gleeful-sprite-55c309.netlify.app/)



### Author

- Frontend Mentor - [@kanuos](https://www.frontendmentor.io/profile/kanuos)
- GitHub - [@ykanuos](https://github.com/kanuos)
- GitHub - [@ykanuos](https://gitlab.com/kanuos)

